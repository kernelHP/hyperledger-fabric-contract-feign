package org.hepeng.fabric.contract.proxy;


import org.junit.Test;

/**
 * @author he peng
 * @date 2022/3/23
 */
public class ContractAPIProxyTest {

    @Test
    public void newProxyInstance() {

        TestContract testContract = ContractProxy.newProxy(TestContract.class , null);

        String s = testContract.txFun1("23213", 1212, true);
    }
}