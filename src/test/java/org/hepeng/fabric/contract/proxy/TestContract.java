package org.hepeng.fabric.contract.proxy;

import org.hepeng.fabric.contract.annotation.ContractAPI;
import org.hepeng.fabric.contract.annotation.TxFunction;

/**
 * @author he peng
 * @date 2022/3/23
 */

@ContractAPI(chainCode = "test-chaincode" , name = "TestContract")
public interface TestContract {

    @TxFunction(argType = TxFunction.ArgType.PRIMITIVE)
    String txFun1(String var1 , Integer var2 , Boolean var3);
}
