package org.hepeng.fabric.contract;

import org.hyperledger.fabric.client.Contract;

/**
 * @author he peng
 * @date 2022/3/24
 */
public interface ContractProvider {

    Contract getContract(String name);
}
