package org.hepeng.fabric.contract;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Set;

/**
 * @author he peng
 * @date 2022/3/23
 */

@Data
@Accessors(chain = true)
public class ContractDefinition {

    String chainCodeName;

    String name;

    Class<?> contractInterface;

    Set<TxFunctionDefinition> txFunctions;
}
