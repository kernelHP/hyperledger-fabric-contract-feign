package org.hepeng.fabric.contract;

import lombok.Data;
import lombok.experimental.Accessors;

import java.lang.reflect.Method;
import java.util.List;


/**
 * @author he peng
 * @date 2022/3/23
 */

@Data
@Accessors(chain = true)
public class TxFunctionDefinition {

    String name;

    Class<?>[] argTypes;

    Boolean query;

    Boolean submit;

    Method method;

    List<String> events;
}
