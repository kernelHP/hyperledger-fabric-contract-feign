package org.hepeng.fabric.contract;

import org.hepeng.fabric.contract.annotation.ContractAPI;

import java.util.HashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

/**
 * @author he peng
 * @date 2022/3/23
 */
public abstract class ChainCodeResolver {

    public static Optional<ChainCodeDefinition> resolve(Class<?> cls) {
        ContractAPI contractAPIAtn = cls.getDeclaredAnnotation(ContractAPI.class);
        if (Objects.isNull(contractAPIAtn)) {
            return Optional.empty();
        }

        Set<ContractDefinition> contracts = new HashSet<>();

        Optional<ContractDefinition> optional = ContractResolver.resolve(cls);
        optional.ifPresent(contracts::add);
        return Optional.of(new ChainCodeDefinition()
                .setName(contractAPIAtn.chainCode())
                .setContracts(contracts));
    }
}
