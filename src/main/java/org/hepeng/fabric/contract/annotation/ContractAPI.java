package org.hepeng.fabric.contract.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author he peng
 * @date 2022/3/23
 */

@Retention(RUNTIME)
@Target(ElementType.TYPE)
@Documented
public @interface ContractAPI {

    String chainCode();

    String name();
}
